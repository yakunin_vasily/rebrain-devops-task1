## Тестовый репозиторий для выполнения задания.

### Содержание:

- Файл nginx.conf, файл создаваемый по-умолчанию при установке .deb пакета в системах debina-like.
- Команды выполняемые для создания этого репозитория. 

** Задать пользователя git глобально **
```
git config --global user.name "Yakunin V. Vasily"
git config --global user.email "me@yakunin.dev"
```

** Для подлючения по ssh, добавить ключ в настройках git, далее создать конфигурационный файл: **
```
vi ~/.ssh/config

Host gitlab.rebrainme.com
	User me@yakunin.dev
	IdentityFile ~/.ssh/yakunin.key
	Compression yes
```

** Инициализация репозитория **
```
cd ~
mkdir rebrain-devops-task1
git init
vi readme.md
vi nginx.conf
git add readme.md
git commit -m 'Init readme.md'
git add nginx.conf
git commit -m 'Init nginx.conf'
git push
git log
git status
```

** Что возможно было сделат так же **
```
Вместо двух коммптов сделать один.
git init
vi readme.md
vi nginx.conf
git commit -a -m 'Init files in git'
git push
git log
git status
Подразумеваем что работаем только с одной веткой, master.
```
